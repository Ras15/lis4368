# LIS4368 - Advanced Web Applications Development

## Remy Santos

### Project #2 Requirements:

*Sub-Heading:*

1. Review cloned lis4368_student_files
2. Include server-side validation from A4
3. Compile the following class and servlet files


#### README.md file should include the following items:

* Provide Screenshots
* Provide git command descriptions


> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - Create an empty Git repository or reinitialize an existing one
2. git status - Show the working tree status
3. git add - Add file contents to the index
4. git commit - Record changes to the repository
5. git push - Update remote refs along with associated objects
6. git pull - Fetch from and integrate with another repository or a local branch
7. One additional git command (git clone) - Clone a repository into a new directory

#### Assignment Screenshots:

*Screenshot of Valid User Form Entry (customerform.jsp)*: | *Screenshot of Passed Validation(thanks.jsp)*:
						  :-------------------------:|:-------------------------:
![Valid User Form Entry](https://bitbucket.org/Ras15/lis4368/raw/2b241f3577dd1418bfe2779c1db7d670d4f79503/p2/img/ValidUserFormEntry.png)| ![Passed Validation](https://bitbucket.org/Ras15/lis4368/raw/2b241f3577dd1418bfe2779c1db7d670d4f79503/p2/img/PassedValidation.png)

*Screenshot of Display Data(customers.jsp)*: | *Screenshot of Modify Form(modify.jsp)*:
						  :-------------------------:|:-------------------------:
![Display Data](https://bitbucket.org/Ras15/lis4368/raw/2b241f3577dd1418bfe2779c1db7d670d4f79503/p2/img/DisplayData.png)| ![Modify Form](https://bitbucket.org/Ras15/lis4368/raw/2b241f3577dd1418bfe2779c1db7d670d4f79503/p2/img/ModifyForm.png)

*Screenshot of Modified Data(customers.jsp)*: | *Screenshot of Delete Warning(customers.jsp)*:
						  :-------------------------:|:-------------------------:
![Modified Data](https://bitbucket.org/Ras15/lis4368/raw/2b241f3577dd1418bfe2779c1db7d670d4f79503/p2/img/ModifiedData.png)| ![Delete Warning](https://bitbucket.org/Ras15/lis4368/raw/2b241f3577dd1418bfe2779c1db7d670d4f79503/p2/img/DeleteWarning.png)

*Screenshot of Associated Database Changes(Select, Insert, Update, Delete)*

![Associated Database Changes](https://bitbucket.org/Ras15/lis4368/raw/2b241f3577dd1418bfe2779c1db7d670d4f79503/p2/img/AssociatedDatabaseChanges.png)
#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
