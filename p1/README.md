# LIS4368 - Advanced Web Applications Development

## Remy Santos

### Project #1 Requirements:

*Deliverables:*

1. Provide Bitbucket read-only access to lis4368 repo, include links to the other assignment repos you created in README.md, using Markdown syntax(README.md) 
2. Must also include screenshots as per above.
3. Blackboard Links:lis4368 Bitbucket repo
4. *Note*: the carousel *must* contain (min. 3) slides that YOU created, that contain text and images that link to other content areas marketing/promoting your skills.

#### README.md file should include the following items:

* Screenshot of main splash page
* Screenshot of failed validation
* Screenshot of passed validation
* Git Command Descriptions

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - Create an empty Git repository or reinitialize an existing one
2. git status - Show the working tree status
3. git add - Add file contents to the index
4. git commit - Record changes to the repository
5. git push - Update remote refs along with associated objects
6. git pull - Fetch from and integrate with another repository or a local branch
7. One additional git command (git clone) - Clone a repository into a new directory

#### Assignment Screenshots:

*Screenshot of main splash page*:

![Splash Page Screenshot](https://bitbucket.org/Ras15/lis4368/raw/77a291ac4d66f5745283f4076918cb87a9974dbe/p1/IMG/Screen%20Shot%202019-03-05%20at%201.07.22%20AM.png)

*Screenshot of failed validation*: 

![Failed Validation](https://bitbucket.org/Ras15/lis4368/raw/77a291ac4d66f5745283f4076918cb87a9974dbe/p1/IMG/Screen%20Shot%202019-03-05%20at%201.09.06%20AM.png)


![Passed Validation](https://bitbucket.org/Ras15/lis4368/raw/77a291ac4d66f5745283f4076918cb87a9974dbe/p1/IMG/Screen%20Shot%202019-03-05%20at%201.11.27%20AM.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
