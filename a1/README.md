# LIS4368 - Advanced Web Applications Development

## Remy Santos

### Assignment #1 Requirements:

*Three Parts:*

1. Distributed Version Control with git and Bitbucket
2. Java/JSP/Servlet Development Installation
3. Chapter Questions (Chps 1-4)

#### README.md file should include the following items:

* Screenshot of running Java Hello (#1 Above)
* Screenshot of running http://localhost:9999 (#2 above)
* git commands w/short descriptions
* Bitbucket repo links a.) This assignment b.) The completed tutorial repo above (bitbucketstationlocations).

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - Create an empty Git repository or reinitialize an existing one
2. git status - Show the working tree status
3. git add - Add file contents to the index
4. git commit - Record changes to the repository
5. git push - Update remote refs along with associated objects
6. git pull - Fetch from and integrate with another repository or a local branch
7. One additional git command (git clone) - Clone a repository into a new directory

#### Assignment Screenshots:

*Screenshot of AMPPS running http://localhost*:

![AMPPS Installation Screenshot](https://bitbucket.org/Ras15/lis4368/raw/b2af1396d6cb9b3688d7465f426dbe33dbd8aae3/A1/ampps_running_SC.png)

*Screenshot of running java Hello*: 

![JavaSS](https://bitbucket.org/Ras15/lis4368/raw/41cf6646960105ed9f2c4b367824dea63c005375/Hello%20World%20Java%20SS.png)


![JDK Installation Screenshot](img/jdk_install.png)

*Screenshot of running http://localhost*:

![tomcat running Screenshot](https://bitbucket.org/Ras15/lis4368/raw/e42ceea05af18afd0b1fdb9e632ad5a45d50d27e/A1/img/tomcat.png)

*A1 index.jsp*:

![tomcat running lis4368](https://bitbucket.org/Ras15/lis4368/raw/b0110cf68c7f1ab372998c89c970ea49db9f4249/A1/img/working_apache_lis4368.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/Ras15/bitbucketstationlocations/src/master/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
