# LIS4368 - Advanced Web Applications Development

## Remy Santos

### Assignment #5 Requirements:

*Sub-Heading:*

 1. Compile Servlet Files
 2. Associated Database Entry
 3. Provide Screenshots
 


#### README.md file should include the following items:

* Screenshot of Valid User Form Entry (customerform.jsp)
* Screenshot of Passed Validation (thanks.jsp)
* Screenshot of Associated Database Entry
* Provide git command descriptions


> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - Create an empty Git repository or reinitialize an existing one
2. git status - Show the working tree status
3. git add - Add file contents to the index
4. git commit - Record changes to the repository
5. git push - Update remote refs along with associated objects
6. git pull - Fetch from and integrate with another repository or a local branch
7. One additional git command (git clone) - Clone a repository into a new directory

#### Assignment Screenshots:

*Screenshot of Valid User Form Entry (customerform.jsp)*: | *Screenshot of Passed Validation(thanks.jsp)*:
						  :-------------------------:|:-------------------------:
![Valid User Form Entry](https://bitbucket.org/Ras15/lis4368/raw/6a9b01f8308e7a68a3710734412cea60843912ef/a5/img/ValidUserFormEntry.png)| ![Passed Validation](https://bitbucket.org/Ras15/lis4368/raw/6a9b01f8308e7a68a3710734412cea60843912ef/a5/img/PassedValidation.png)

*Screenshot of Associated Database Entry*:
								 
![Valid User Form Entry](https://bitbucket.org/Ras15/lis4368/raw/1931361a5d73adcb839289aa114fd63578814ee1/a5/img/Associated%20Database%20Entry.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
