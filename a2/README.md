# LIS4368 - Advanced Web Applications Development

## Remy Santos

### Assignment #2 Requirements:

*Sub-Heading:*

1. Development Installation: MySQLClient Login Using AMPPS
2. Finish Tomcat Tutorial


#### README.md file should include the following items:

* Assessment links
* Only *one* screenshot of the query results from the following link (see screenshot below): http://localhost:9999/hello/querybook.html

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - Create an empty Git repository or reinitialize an existing one
2. git status - Show the working tree status
3. git add - Add file contents to the index
4. git commit - Record changes to the repository
5. git push - Update remote refs along with associated objects
6. git pull - Fetch from and integrate with another repository or a local branch
7. One additional git command (git clone) - Clone a repository into a new directory

#### Assignment Screenshots:

*Screenshot of http://localhost:9999/hello*:

![http://localhost:9999/hello Screenshot](https://bitbucket.org/Ras15/lis4368/raw/ba52d79527a093477eeae546e21971075ba70860/A2/img/hello_sc.png)

*Screenshot of http://localhost:9999/hello/index.html*: 

![http://localhost:9999/hello/index.html SS](https://bitbucket.org/Ras15/lis4368/raw/ba52d79527a093477eeae546e21971075ba70860/A2/img/hello%3Aindex.html_sc.png)

*Screenshot of http://localhost:9999/hello/sayhello*:

![http://localhost:9999/hello/sayhello Screenshot](https://bitbucket.org/Ras15/lis4368/raw/ba52d79527a093477eeae546e21971075ba70860/A2/img/hello%3Asayhello_sc.png)

*Screenshot of http://localhost:9999/hello/querybook.html*:

![http://localhost:9999/hello/querybook.html Screenshot](https://bitbucket.org/Ras15/lis4368/raw/ba52d79527a093477eeae546e21971075ba70860/A2/img/hello%3Aquerybook_sc.png)

*Screenshot of http://localhost:9999/hello/sayhi*:

![http://localhost:9999/hello/sayhi Screenshot](https://bitbucket.org/Ras15/lis4368/raw/ba52d79527a093477eeae546e21971075ba70860/A2/img/hello%3Asayhi_sc.png)

*Screenshot of http://localhost:9999/hello/query?author=Tan+Ah+Teck*:

![http://localhost:9999/hello/query?author=Tan+Ah+Teck* Screenshot](https://bitbucket.org/Ras15/lis4368/raw/ba52d79527a093477eeae546e21971075ba70860/A2/img/hello%3Aqueryresults_sc.png)



#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
