# LIS4368 - Advanced Web Applications Development

## Remy Santos

### Assignment #4 Requirements:

*Sub-Heading:*

1. Download CustomerServlet.java
2. Compile Servlet Files
3. Screenshots

#### README.md file should include the following items:

* Server-side Validation
* Screenshot of Failed Validation
* Screenshot of Passed Validation
* Provide git command descriptions

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - Create an empty Git repository or reinitialize an existing one
2. git status - Show the working tree status
3. git add - Add file contents to the index
4. git commit - Record changes to the repository
5. git push - Update remote refs along with associated objects
6. git pull - Fetch from and integrate with another repository or a local branch
7. One additional git command (git clone) - Clone a repository into a new directory

#### Assignment Screenshots:

*Screenshot of Failed Validation*:

![A4 Screenshot](https://bitbucket.org/Ras15/lis4368/raw/c344a1fb0c91454a8aca1f316c596fe171f4ee60/a4/Img/A4ErrorValidation.png)

*Screenshot of Passed Validation*: 

![A4 SS](https://bitbucket.org/Ras15/lis4368/raw/c344a1fb0c91454a8aca1f316c596fe171f4ee60/a4/Img/A4PassedValidation.png)




#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
