# LIS4368 - Advanced Web Applications Development

## Remy Santos

### Assignment #3 Requirements:

*Deliverables:*

1. Entity Relationship Diagram (ERD)
2. Include Data (at least 10 records each table)
3. Provide Bitbucket read-only access to repo (Language SQL), must include README.md, using Markdown syntax, and inlcude links to all of the following files (from README.md):
	* docs folder: a3.mwb, and a3.sql
	* img folder: a3.png (export a3.mwb file as a3.png)
	* README.md (MUST display a3.png ERD)
	* Blackboard links: Bitbucket repo

#### README.md file should include the following items:

* Screenshot of ERD that links to the image


> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - Create an empty Git repository or reinitialize an existing one
2. git status - Show the working tree status
3. git add - Add file contents to the index
4. git commit - Record changes to the repository
5. git push - Update remote refs along with associated objects
6. git pull - Fetch from and integrate with another repository or a local branch
7. One additional git command (git clone) - Clone a repository into a new directory

#### Assignment Screenshots:

*Screenshot of A3 ERD*:

![A3 ERD Screenshot](https://bitbucket.org/Ras15/lis4368/raw/58d5755002d5f29d614a9fadbc51acb80b4416ca/a3/img/LIS4368_A3_ERD.png)

*A3 docs: a3.mwb and a3.sql*:
[A3 MWB File](docs/a3.mwb "A3 ERD in .mwb format")
[A3 SQL File](docs/a3.sql "A3 SQL Script")

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
