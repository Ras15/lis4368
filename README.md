# LIS4368 - Advanced Web Applications Development

## Remy Santos 

### LIS4368 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install JDK
    - Install tomcat
    - Provide Screenshots of installations
    - Create Bitbucket repo
    - Complete Bitbucket tutorials
    (bitbucketstationlocations and myteamquotes)
    - Provide git command descriptions

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - MySQLClient Login Using AMPPS:
    - Finish the tomcat tutorial:
    - Provide git command descriptions

3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Install MYSQL Workbench
    - MWB file must forward engineer
    - Provide Screenshot of ERD
    - Provide git command descriptions

4. [A4 README.md](a4/README.md "My A4 README.md file")
    - Download CustomerServlet.Java
    - Compile Servlet Files
    - Provide Screenshots
    - Provide git command descriptions
    
5. [A5 README.md](a5/README.md "My A5 README.md file")
    - Compile Servlet Files
    - Associated Database Entry
    - Provide Screenshots
    - Provide git command descriptions
    
6. [P1 README.md](p1/README.md "My P1 README.md file")
    - Review cloned lis4368_student_files
    - Open index.jsp and review code
    - Research Validation Codes
    - Form Validation Plugin
    - Lesson 6
    - Lesson 7

7. [P2 README.md](p2/README.md "My P2 README.md file")
    - Review cloned lis4368_student_files
    - Include server-side validation from A4
    - Compile the following class and servlet files
    - Provide Screenshots
    - Provide git command descriptions


